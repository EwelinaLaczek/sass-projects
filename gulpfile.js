var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var browserSync = require('browser-sync').create();

gulp.task('sass', function(){
    return gulp.src('app/scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
        .pipe(livereload({start: true}))
});

gulp.task('watch', ['sass', 'browserSync'], function(){
    livereload.listen();
    //watching changes in scss file
    gulp.watch('app/scss/**/*.scss', ['sass']);
});

gulp.task('browserSync', function(){
    browserSync.init({
        server: {
            baseDir: 'app'
        }
    })
});